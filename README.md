# This project includes ShoppingHelper Android application

## Author: Matija Almaši

The purpose of this application is to allow users to compare prices of product
across various stores by scanning the barcode of the product.